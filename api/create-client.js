const AWS = require('aws-sdk');
const SNS = require('aws-sdk/clients/sns')
const DYNAMODB = require("aws-sdk/clients/dynamodb");

const dynamodb = new DYNAMODB({region: "us-east-1"});
const sns = new SNS({ region: "us-east-1" });
const tableData = process.env.DB_TABLE_NAME;
const snsData = process.env.SNS_ARN;

exports.handler = async (event) => {
    const eventBody = JSON.parse(event.body);
    if (!eventBody || !eventBody.dni || !eventBody.birthDay || !eventBody.firstName || !eventBody.lastName ) {
        return {
            statusCode: 400,
            body: "No attributes found"
        }
    }
    if (controlAge(eventBody.birthDay) > 65){
        return {
            statusCode: 400,
            body: "Older people are not allowed"
        }
    }
    const params = {
        Item: {
            dni: {
                S: eventBody.dni
            },
            birthDay: {
                S: eventBody.birthDay
            },
            firstName: {
                S: eventBody.firstName
            },
            lastName: {
                S: eventBody.lastName
            },
        },
        ReturnConsumedCapacity: "TOTAL",
        TableName: tableData
    };
    const clientCreated = await putItem(params);
    if (!clientCreated) {
        return {
            statusCode: 400,
            body: "Could not save client"
        }
    }
    const snsPublished = await publishSNS(eventBody);
    if (!snsPublished) {
        return {
            statusCode: 400,
            body: "Could not send sns"
        }
    }
    const response = {
        statusCode: 200,
        body: "Client created succesfully"
    };
    return response;
}

const publishSNS = (params) => {
    const snsParams = {
        Message: JSON.stringify(params),
        TopicArn: snsData,
    };
    try {
        const snsResponse = sns.publish(snsParams).promise();
        return snsResponse;
    } catch (error) {
        console.log('CATCH - publishSNS ', error);
        return false;
    }
}

const putItem = (params) => {
    try {
        const dbResult = dynamodb.putItem(params).promise();
        return dbResult;
    } catch (error) {
        console.log('CATCH - putItem ', error);
        return false;
    }
}

const controlAge = (data) => {
    const today = new Date()
    let birthDay = new Date(data)
    let controledAge = today.getFullYear() - birthDay.getFullYear()
    return controledAge;
}